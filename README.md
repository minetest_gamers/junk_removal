## Minetest Mod: Junk Removal by SaKeL

Remove nodes from players what are not anymore listed in the /players directory. Protector, Protector2, locked doors, locked trapdoors, protected chests, basic machines, enchantment tables, easy vending machines, city blocks and locked chest will be removed by defautlt. It's easy to edit what blocks should be removed from init.lua

Also removes new players what will leave the game without having by default: { "default:stick 1", "default:steel_ingot 1" } in the inventory.
The items can be easily modified. Player file from /players directory will be removed and (after server shutdown) also player data from auth.txt will be removed.
After the player leaves the game with satisfied requirements (items in inventory), this player inventory will not be checked any more. So it's just for new players and only for the 1st time they have those items in invetory.
Prevents /players folder and auth.txt getting cluttered with players what only comes to the server to have a look or not really doing any work at the server.

Tested on Linux server, for Windows you have to adjust the paths from common slash "/" to backslash "\".